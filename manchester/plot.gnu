set title 'grafico sinal digital Manchester'
set key above
set grid
set xlabel 'eixo X'
set ylabel 'eixo Y'
set yrange[-2:2]
set xrange[-1:20]
set terminal png medium size 600,400 enhanced;
set output 'graficoA.png'
plot "entManchester0" w lines
set output 'graficoB.png'
plot "entManchester1" w lines
set output 'graficoC.png'
plot "entManchester2" w lines
set output 'graficoD.png'
plot "entManchester3" w lines
set output 'graficoE.png'
plot "entManchester4" w lines
set output 'graficoF.png'
plot "entManchester5" w lines
set output 'graficoG.png'
plot "entManchester6" w lines
set output 'graficoH.png'
plot "entManchester7" w lines
set output 'graficoI.png'
plot "entManchester8" w lines
set output 'graficoJ.png'
plot "entManchester9" w lines