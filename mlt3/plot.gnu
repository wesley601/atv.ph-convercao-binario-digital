set title 'grafico sinal digital Mlt3'
set key above
set grid
set xlabel 'eixo X'
set ylabel 'eixo Y'
set yrange[-2:2]
set xrange[-1:10]
set terminal png medium size 600,400 enhanced;
set output 'graficoA.png'
plot "entMLT30" w lines
set output 'graficoB.png'
plot "entMLT31" w lines
set output 'graficoC.png'
plot "entMLT32" w lines
set output 'graficoD.png'
plot "entMLT33" w lines
set output 'graficoE.png'
plot "entMLT34" w lines
set output 'graficoF.png'
plot "entMLT35" w lines
set output 'graficoG.png'
plot "entMLT36" w lines
set output 'graficoH.png'
plot "entMLT37" w lines
set output 'graficoI.png'
plot "entMLT38" w lines
set output 'graficoJ.png'
plot "entMLT39" w lines