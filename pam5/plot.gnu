set title 'grafico sinal digital PAM-5'
set key above
set grid
set xlabel 'eixo X'
set ylabel 'eixo Y'
set yrange[-3:3]
set xrange[-1:10]
set terminal png medium size 600,400 enhanced;
set output 'graficoA.png'
plot "pam50" w lines
set output 'graficoB.png'
plot "pam51" w lines
set output 'graficoC.png'
plot "pam52" w lines
set output 'graficoD.png'
plot "pam53" w lines
set output 'graficoE.png'
plot "pam54" w lines
set output 'graficoF.png'
plot "pam55" w lines
set output 'graficoG.png'
plot "pam56" w lines
set output 'graficoH.png'
plot "pam57" w lines
set output 'graficoI.png'
plot "pam58" w lines
set output 'graficoJ.png'
plot "pam59" w lines