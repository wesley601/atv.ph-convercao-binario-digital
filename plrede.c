#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char manchester(int len, char str[], int qtArq){
    FILE *manc_file;
    char cQtArq = qtArq+'0';
    double x1 = 1, x2 = 1.5;
    char filename[] = {'e','n','t','M','a','n','c','h','e','s','t','e','r',cQtArq};


    manc_file = fopen(filename,"a");

    for(int i = 0; i < len; i++)
    {
       if(str[i] == '1'){

           fprintf(manc_file, "%f -1 \n%f -1 \n", x1, x2);
          
           fprintf(manc_file, "%f 1 \n", x2);
           x1++;
           x2++;
           fprintf(manc_file, "%f 1 \n", x1);
       } 
       else
        {
           fprintf(manc_file, "%f 1 \n%f 1 \n", x1, x2);
           
           fprintf(manc_file, "%f -1 \n", x2);
           x1++;
           x2++;
           fprintf(manc_file, "%f -1 \n", x1);
       }
    }
    fclose(manc_file);
}
char mlt3(int len, char str[], int qtArq){
    FILE *mlt_file;
    //double x1 = 0, x2 = 0.5;
    char cQtArq = qtArq+'0';
    char filename[] = {'e','n','t','M','L','T','3',cQtArq};

    double x = 0;
    double y = 0;
    double ap = -1;

    mlt_file = fopen(filename,"a");
    
    fprintf(mlt_file,"%f %f \n", x, y);
        
    for(int i = 0; i < len; i++){
        if(str[i] == '1'){
            if(y == 1){
                ap = y;
                y--;
            } else if(y == -1){
                    ap = y;
                    y++;
                } else if(y == 0){
                        if(y > ap){
                            y++;
                        } else {
                            y--;
                        }
                    }    
        fprintf(mlt_file,"%f %f \n", x, y);
        x+=0.5;
        fprintf(mlt_file,"%f %f \n", x, y);
        } else {
            x+=0.5;
            fprintf(mlt_file,"%f %f \n", x, y);
        }
    }
    fclose(mlt_file);
}
char pam5(int len, char str[], int qtArq){
    FILE *pam_file;
    char cQtArq = qtArq+'0';
    char filename[] = {'p','a','m','5',cQtArq};
    pam_file = fopen(filename,"a");
    
    char par[2];
    double x = 0;
    double y = 0;
    int i = 0;
    
    while(i < len){
        for(int j = 0; j < 2; j++){par[j] = str[j+i];}
        
        
        printf("%c %c\n ", str[i], str[i+1]);
        
         if(str[i] == '0' && str[i+1] == '0')
         {
             y = 1;
             fprintf(pam_file,"%f %f \n",x ,y);
             x++;
             fprintf(pam_file,"%f %f \n",x, y);
         }
          else if(str[i] == '1' && str[i+1] == '0')
         {
             y = -1;
             fprintf(pam_file,"%f %f \n",x ,y);
             x++;
             fprintf(pam_file,"%f %f \n",x, y);
         } 
         else if(str[i] == '0' && str[i+1] == '1')
         {
             y = 2;
             fprintf(pam_file,"%f %f \n",x ,y);
             x++;
             fprintf(pam_file,"%f %f \n",x, y);
         } 
         else if(str[i] == '1' && str[i+1] == '1')
         {
             y = -2;
             fprintf(pam_file,"%f %f \n",x ,y);
             x++;
             fprintf(pam_file,"%f %f \n",x, y);
         }
        i+=2;
    }
    

    fclose(pam_file);
    }
    
int main(int argc, char const *argv[])
{
    FILE *bins;
    char ch;
    char bin[50];
    bins = fopen("bins","r");
    
    int qtArq = 0; // quantidade de arquivos

    while(ch=fgetc(bins) != EOF)
    {
        fscanf(bins,"%s", bin);
        //manchester(strlen(bin), bin, qtArq);
        //mlt3(strlen(bin), bin, qtArq);
        pam5(strlen(bin), bin, qtArq);
        qtArq++;
        printf("%d\n", qtArq);
    }
    fclose(bins);
    printf("vê aí se deu certo \n");
    return 0;
}
